Refinery::Core::Engine.routes.append do

  # Frontend routes
  namespace :footers do
    resources :footers, :path => '', :only => [:index, :show]
  end

  # Admin routes
  namespace :footers, :path => '' do
    namespace :admin, :path => 'refinery' do
      resources :footers, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
