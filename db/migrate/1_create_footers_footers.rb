class CreateFootersFooters < ActiveRecord::Migration

  def up
    create_table :refinery_footers do |t|
      t.string :title
      t.text :content
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-footers"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/footers/footers"})
    end

    drop_table :refinery_footers

  end

end
