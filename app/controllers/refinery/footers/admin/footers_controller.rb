module Refinery
  module Footers
    module Admin
      class FootersController < ::Refinery::AdminController

        crudify :'refinery/footers/footer', :xhr_paging => true

      end
    end
  end
end
